import os
import compileall
import glob
import shutil
from zipfile import ZipFile

VERSION="1.5.688"
SOURCE_CODE = os.getcwd()
# SOURCE_CODE = os.path.dirname(SOURCE_CODE) + "\\dat_release"
# BUILD_DIR = SOURCE_CODE+"\\%s"%VERSION
BUILD_DIR =os.path.dirname(SOURCE_CODE)+"\\dat_release\\%s"%VERSION

# sudo nano /var/spool/cron/crontabs/pi
# @reboot sleep 10 && DISPLAY=:0 /home/pi/Documents/main-app.sh start;

def copy_source(source_path, build_patch):
    list_fol = [f.path for f in os.scandir(source_path) if f.is_dir()]
    for fol_path in list_fol:
        fol_name = os.path.basename(fol_path)
        if fol_name == '.git' or fol_name == 'venv':
            print('Do not copy folder: ', fol_name)
            continue
        shutil.copytree(fol_path,build_patch+"\\"+fol_name)
        print ('Copy ', fol_path)

    list_file = [f.path for f in os.scandir(source_path) if not f.is_dir()]
    print('==================Copy list file ======================')
    for file_path in list_file:
        file_name = os.path.basename(file_path)
        shutil.copyfile(file_path,build_patch + '\\'+file_name)
        print('Copy ' ,file_path)
    print('Copy successfully ',len(list_fol), ' folder and ',len(list_file), ' file')


def clean_pycache(dir_path):
    print("Clean pycache ")
    pyc_files = glob.glob(dir_path + '\\**\\*.pyc', recursive=True)

    for f in pyc_files:
        try:
            os.remove(f)
            print("Remove :", f)
        except OSError as e:
            print("Error: %s : %s" % (f, e.strerror))


def gen_pyc_file(dir_path):
    print("Clean PY file")
    pyc_files = glob.glob(dir_path + '\\**\\*.pyc', recursive=True)

    for pyc_f in pyc_files:
        src = pyc_f
        dst_path, dst_file = pyc_f.split("\\__pycache__\\")
        dst = dst_path + "\\" + dst_file.split(".")[0]+ ".pyc"
        shutil.copyfile(src, dst)
        print("Created :", dst)
        py_file = dst.replace(".pyc", ".py")
        try:
            if os.path.basename(py_file) == 'config.py':
                print('Bo qua, khong remove file ' + os.path.basename(py_file))
                continue
            os.remove(py_file)
            print("Removed :", py_file)
        except OSError as e:
            print("Error: %s : %s" % (py_file, e.strerror))


def get_all_file_paths(directory):
    # initializing empty file paths list
    file_paths = []
    # crawling through directory and subdirectories
    for root, directories, files in os.walk(directory):
        for filename in files:
            # join the two strings in order to form the full filepath.
            file_paths.append(os.path.join(root, filename))
    return file_paths

def zipfile(directory):
    print ('|===== Zip file =========================================')
    # calling function to get all file paths in the directory
    file_paths = get_all_file_paths(directory)

    # # printing the list of all files to be zipped
    # for file_name in file_paths:
    #     print(file_name)

    # writing files to a zipfile
    pathzip = os.path.dirname(directory)
    print ('directory:', directory)
    print('pathzip: ', pathzip)
    namezip = pathzip+ '\\' +os.path.basename(directory)+ '.zip'
    print('namezip: ', namezip)
    with ZipFile(namezip, 'w') as zip:
        # writing each file one by one
        for file in file_paths:
            print('=>', file)
            zip.write(file)
    print(namezip, ' file is created successfully!')

if __name__ == '__main__':
    if os.path.exists(BUILD_DIR):
        try:
            shutil.rmtree(BUILD_DIR)
            print('removed ',BUILD_DIR)
        except:
            print('Can not remove ', BUILD_DIR)
            pass
    print("Make a copy :", SOURCE_CODE, "-->", BUILD_DIR)
    copy_source(SOURCE_CODE,BUILD_DIR)
    # destination = shutil.copytree(SOURCE_CODE, BUILD_DIR)
    # print("Finish copy source code to ", destination)
    clean_pycache(BUILD_DIR)
    print("Compile ALL SOURCE CODE DIR: ", BUILD_DIR)
    compileall.compile_dir(BUILD_DIR, force=True)
    gen_pyc_file(BUILD_DIR)
    print("Success build source code to DIRECTOR: ", BUILD_DIR)

    # zipfile(BUILD_DIR)




