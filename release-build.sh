#! /bin/bash
echo "Start compile pyc file";
python -m compileall .;
echo "End compile pyc file";
echo "Start zip file and release";
find . -name '*.py' -delete;
zip -r release/release-101.zip .;
git checkout .;
echo "End zip file and release";
