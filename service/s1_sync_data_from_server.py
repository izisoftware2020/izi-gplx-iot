import os, sys
sys.path.append(os.getcwd())
import base64

# Turn off bytecode generation
# https://docs.djangoproject.com/en/3.0/topics/db/models/
sys.dont_write_bytecode = True
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")
import django
django.setup()
from database.models import *
import requests
import xmltodict
import pprint
import json
import socket

from modules.m9_sound import *

URL = "http://14.241.134.20:8082/api/thietbi/thethuexe/ravao"
headers = {'content-type': 'text/xml'}

def check_connect_server(rm_server):
    try:
        # see if we can resolve the host name -- tells us if there is
        # a DNS listening
        if rm_server is None:
            rm_server = REMOTE_SERVER
        host = socket.gethostbyname(rm_server)
        # connect to the host -- tells us if the host is actually
        s = socket.create_connection((host, 80), 2)
        s.close()
        return True
    except:
        pass
    return False

# 0001.000002.0001
def check_in_check_out(ma_thiet_bi="1111111555"): 
    
    try: 
        print('start checkout')
        body = {"maThe": ma_thiet_bi}
        headers = {'content-type': 'application/json', "Content-Length": "255"}

        print(json.dumps(body))
  
        # sending get request and saving the response as response object
        r = requests.post(URL, data=json.dumps(body), headers=headers) 
        print(URL, json.dumps(body))

        # extracting data in json format
        data = r.json()
        print(data)
        if data['status'] == 1:
            play_quet_the_thanh_cong()
        elif data['status'] == 2:
            play_quet_the_that_bai()
        elif data['status'] == 3:
            play_so_tien_khong_du()
        
        print('end checkout')

    except Exception as e:
        print(e)
        pass
    return False, None
 
 
# check_in_check_out('1')
