# Hướng Dẫn cài đặt tự động start DCOM và ứng dụng 

- Copy tất cả file trong thư mục `deploy` vào trong thư mục `/home/pi/Documents`:
- Config crontab cho tự động chạy: 
   1. open crontab: `crontab -e`
   2. Thêm những đoạn mã tự động chạy lúc khởi động raspberry Pi :
```
  @reboot sleep 5 && sudo sh /home/pi/Documents/start_dcom.sh > /home/pi/Documents/Logs/start_log.txt
  @reboot sleep 70 && DISPLAY=:0 /home/pi/Documents/main-app.sh start >> /home/pi/Documents/Logs/start_log.txt
```

Open lại Crontab để config lại startup
```
sudo nano /var/spool/cron/crontabs/pi
```
