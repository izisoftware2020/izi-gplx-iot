from pytz import utc
from subprocess import call
import time
import os, sys 

sys.path.append(os.getcwd())

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import *
from apps.a1_login.a11_login_teacher import LoginTeacher

from apscheduler.schedulers.background import BackgroundScheduler

# Turn off bytecode generation
# https://docs.djangoproject.com/en/3.0/topics/db/models/
sys.dont_write_bytecode = True
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")
import django
django.setup()
from database.models import *
from config.config import MaDV,useAuthenticate

# init Pulse class    
from modules.m11_rasbery_pi import *
from service.s1_sync_data_from_server import * 

from modules.m3_rfid import RFID_CR522U
from modules.m0_common import COMMON 

import threading
 
def call_ui():  
    check_in_out()   
    
def check_in_out():    
    common = COMMON.instance() 

    rfid = RFID_CR522U()
    common.enableEthernet(True)
    common.AllowRC522broad() 
    
    has_login, status, KeyWord, GiaoVienID, ma_thiet_bi = rfid.LoginRFID()
    print(datetime.now().strftime("%H:%M:%S %f"), 'Try Login student with rfid ', status)
    
    if not has_login:
        threading.Timer(1.0, check_in_out).start()
        return 

    print('ma_thiet_bi', ma_thiet_bi)
    check_in_check_out(str(int(ma_thiet_bi))) 
    time.sleep(4) 
    threading.Timer(1.0, check_in_out).start()

def discovery_device(): 
    # ret = True
    info_device = ThongTinThietBi() 
    if len(ThongTinThietBi.objects.all()) > 0:
        info_device = ThongTinThietBi.objects.first()
    info_device.save() 

    DiscoveryRFID(info_device)  
    DiscoveryServer(info_device,REMOTE_SERVER)
    info_device.save() 
  
def init_data_login():

    discovery_device()  
    call_ui()  
