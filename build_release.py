#! /bin/bash
import os
import compileall
import glob
import shutil

VERSION="1.5.688"
SOURCE_CODE = os.getcwd()
BUILD_DIR =os.path.dirname(SOURCE_CODE)+"/release_%s"%VERSION


def clean_pycache(dir_path):
    print("Clean PYCATCHE ")
    pyc_files = glob.glob(dir_path + '/**/*.pyc', recursive=True)

    for f in pyc_files:
        print("Remove :", f)
        try:
            os.remove(f)
        except OSError as e:
            print("Error: %s : %s" % (f, e.strerror))


def clean_py(dir_path):
    print("Clean PY file")
    pyc_files = glob.glob(dir_path + '/**/*.pyc', recursive=True)

    for pyc_f in pyc_files:
        print(pyc_f)
        src = pyc_f
        dst_path, dst_file = pyc_f.split("/__pycache__/")
        dst = dst_path + "/" + dst_file.split(".")[0]+ ".pyc"
        print("DEBUG:", dst)
        shutil.copyfile(src, dst)
        py_file = dst.replace(".pyc", ".py")
        print("Remove :", py_file)
        try:
            os.remove(py_file)
        except OSError as e:
            print("Error: %s : %s" % (py_file, e.strerror))


if __name__ == '__main__':
    print("Create BUILD DIR:", BUILD_DIR)
    try:
        shutil.rmtree(BUILD_DIR)
    except:
        pass
    print("Make a copy :", SOURCE_CODE, "-->", BUILD_DIR)
    destination = shutil.copytree(SOURCE_CODE, BUILD_DIR)
    print("Finish copy source code to ", destination)
    clean_pycache(BUILD_DIR)
    print("Compile ALL SOURCE CODE DIR: ", BUILD_DIR)
    compileall.compile_dir(BUILD_DIR, force=True)
    clean_py(BUILD_DIR)
    print("Success build source code to DIRECTOR: ", BUILD_DIR)





