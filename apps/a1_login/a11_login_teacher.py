# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'WaitScreen.ui'
#
# Created by: PyQt5 UI code generator 5.14.2
#
# WARNING! All changes made in this file will be lost!
import datetime
import os, sys
from os import path
import time
import requests
import RPi.GPIO as GPIO

from PyQt5.QtCore import QTimer


sys.path.append(os.getcwd())
os.environ['TZ'] = 'Asia/Bangkok'

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *

# Turn off bytecode generation
# https://docs.djangoproject.com/en/3.0/topics/db/models/
sys.dont_write_bytecode = True
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")
import django

django.setup()
from database.models import *

from modules.m3_rfid import RFID_CR522U
from modules.m0_common import COMMON 

from config.config import TimeLogin, useAuthenticate

from service.s1_sync_data_from_server import check_in_check_out

TimeWaiting = TimeLogin * 1 # miliseconds


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1024, 600)
         

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
          
        # check finger
        self.btnShutdown.clicked.connect(MainWindow.shutdown)


class LoginTeacher(QMainWindow):
    def __init__(self):
        super().__init__()
        # self.ui = Ui_MainWindow()
        # self.ui.setupUi(self)

        self.common = COMMON.instance()
        # clear data
        self.GiaoVienID = ''

        self.target_setting_file = '/home/pi/Documents/setting.txt'
 

        self.rfid = RFID_CR522U()
        self.common.enableEthernet(True)
        self.common.AllowRC522broad()

        # login with RFID
        self.timer_login_teacher = QtCore.QTimer(self)
        self.timer_login_teacher.timeout.connect(self.take_login_teacher)
        self.timer_login_teacher.start(500) 
 
  
    def take_login_teacher(self):
        # self.timer_login_teacher.stop()

        if useAuthenticate == 1:
            try:
                has_login, status, KeyWord, GiaoVienID, ma_thiet_bi = self.rfid.LoginRFID()
                print(datetime.now().strftime("%H:%M:%S %f"), 'Try Login student with rfid ', status)
                
                self.setConected_Rfid(True)
                if not has_login:
                    # self.timer_login_teacher.start(TimeWaiting)
                    return 

                print('ma_thiet_bi', ma_thiet_bi)
                check_in_check_out(str(int(ma_thiet_bi))) 
                time.sleep(5)
                # self.timer_login_teacher.start(TimeWaiting)

                
            except Exception as ex:
                print(datetime.now().strftime("%H:%M:%S %f"),'Không kết nối với RFID Reader1: ', ex)
                write_log('a1_login_teacher.py', 'take_login_teacher', 'Không kết nối với RFID Reader', 'error')
                self.setConected_Rfid(False)
                # self.timer_login_teacher.start(TimeWaiting)
                return
           
    def shutdown(self):
        self.timer_login_teacher.stop()
        print(datetime.now().strftime("%H:%M:%S"), 'shutdown')
        self.common.enableEthernet(False)

        sys.exit()
        # command = "/usr/bin/sudo /sbin/shutdown -r now"
        # import subprocess
        # process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
        # output = process.communicate()[0]
        # print(output)

    def restart(self):
        import os
        os.system('sudo reboot')

    def setConected_Rfid(self, trangthai):
        self.device_info = ThongTinThietBi.objects.first()
        self.device_info.DeviceRFIDStatus = trangthai
        self.device_info.save()

    def setConected_Fingerprint(self, trangthai):
        self.device_info = ThongTinThietBi.objects.first()
        self.device_info.DeviceFingerStatus = trangthai
        self.device_info.save()
        self.check_status_device()
  

if __name__ == "__main__":
    import sys

    app = QtWidgets.QApplication(sys.argv)
    w = LoginTeacher()
    w.show()
    sys.exit(app.exec_())
