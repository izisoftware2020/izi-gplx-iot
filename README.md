# izi_iot
###### Project iot
###### ├── apps
###### │   └── app_sample
###### │       ├── admin.py
###### │       ├── apps.py
###### │       ├── __init__.py
###### │       ├── migrations
###### │       │   └── __init__.py
###### │       ├── models.py
###### │       ├── tests.py
###### │       └── views.py
###### │   └── a1_login 
###### │   └── a2_dashboard 
###### │   └── a3_status 
###### │   └── a3_logout 
###### │   └── a4_speed 
###### │   └── a5_sync_server_local 
###### │   └── a6_setting 
###### │   └── a7_search 
###### │   └── a8_add_student 
###### │   └── a9_warning  
###### ├── django_project
###### │   ├── __init__.py
###### │   ├── __pycache__
###### │   │   ├── __init__.cpython-35.pyc
###### │   │   └── settings.cpython-35.pyc
###### │   ├── settings.py
###### │   ├── urls.py
###### │   └── wsgi.py
###### ├── manage.py
###### ├── media
###### │   ├── face
###### │   ├── fingers
###### │   │   └── teachers
###### │   │   └── students
###### ├── modules
###### │   ├── m1_screen.py
###### │   ├── m2_camera.py
###### │   ├── m3_rfid.py
###### │   ├── m4_finger.py
###### │   ├── m5_locate.py
###### │   ├── m6_dcom.py
###### │   ├── m7_pulse.py
###### │   ├── m8_microsd.py
###### │   ├── m9_sound.py
###### │   ├── m10_power.py 
###### └── schedules
###### │   ├── s1_check.py
###### │   ├── s2_sync_history_to_server.py
###### ├── static
###### └── templates
###### 
###### 
###### => basic design
###### modules
###### 	screen
###### 	camera
###### 	rfid
###### 	finger
###### 	locate
###### 	dcom
###### 	pulse
###### 	microsd
###### 	sound
###### 	power 
###### apps
###### 	login
###### 		=> đăng nhập giáo viên, học viên
###### 	dashboard
###### 		=> hiển thị menu các chức năng
###### 			status, logout, speed, sync_server_local, setting, search, add_student
###### 	status
###### 		=> hiển thị trạng thái hoạt động của các thiết bị
###### 	logout
###### 		=> cho phép 2 loại đăng xuất => đăng xuất tất cả hoặc đăng xuất học viên => đăng xuất giáo viên
###### 	speed
###### 		=> hiển thị quảng đường và tốc độ của xe
###### 	sync_server_local
###### 		=> đồng bộ database giửa local và server
###### 	setting
###### 		=> cài đặt các tham số trên thiết bị
###### 	search
###### 		=> tìm kiếm thông tin học viên dựa vào cmnd
###### 	add_student
###### 		=> thêm học viên ở local 
###### 	warning
###### 		=> cảnh báo không phải người đang lái xe 
###### schedules
###### 	check
###### 		check_people_with_camera
###### 		check_speed_with_pulse
###### 		check_power
###### 		check_...
###### 		
###### 	sync_history_to_server 
"""
python -m PyQt5.uic.pyuic -x status.ui -o status.py ; python status.py
"""

import data to database
mysql -u root -p izi_iot_db < izi_iot_db.sql

## Prepare dev environment
```
conda create -n izi python=3.6 jupyter ipykernel -y

$ pip3 install opencv-contrib-python==4.1.0.25
$ pip3 install Pyqt5
$ pip3 install dlib face_recognition imutils

```

### Create face to database
```
1. copy image to directory : media/faces/ 
 Note: Make name image is number. example: 2.jpg
2. Load image to NguoiLXFace database
$ python database/m2_create_NguoiLXFace.py
```

```
# create database for face user
$ python database/m2_create_NguoiLXFace.py

```

## Usage
```batch

$ python apps/a1_login/a11_login_teacher.py
```

