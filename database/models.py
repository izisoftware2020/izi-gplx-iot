import sys
from datetime import datetime

try:
    from django.db import models
except Exception:
    print("Exception: Django Not Found, please install it with \"pip install django\".")
    sys.exit()


class CurrentLogin(models.Model):
    Id = models.AutoField(primary_key=True)
    MaCSDT = models.CharField(null=True, max_length=50, default="")
    Imei = models.CharField(null=True, max_length=50, default="")
    Token = models.CharField(null=True, max_length=50, default="")
    IsGiaoVienLogin = models.BooleanField(null=True, default=False)
    GiaoVienID = models.CharField(null=True, max_length=50, default="")
    IsLaiXeLogin = models.BooleanField(null=True, default=False)
    NguoiLXID = models.CharField(null=True, max_length=50, default="")
    KhoaHocID = models.CharField(null=True, max_length=50, default="")
    VehicleID = models.CharField(null=True, max_length=50, default="")
    IsSetting = models.BooleanField(null=True, default=False)

    class Meta:
        db_table = "CurrentLogin"

class DonViGTVT_Curent(models.Model):
    MaDV = models.CharField(null=True, max_length=6, default="")
    TenDV = models.CharField(null=True, max_length=100, default="")
    Logo = models.BinaryField(null=True, default=b'')
    # xác thực cho học viên 1 = RFID, 2 = Finger
    Auth = models.IntegerField(null=True, default=0)
    # số xung để quy đổi ra mét
    Pulse = models.CharField(null=True, max_length=15, default="")
    # config loại GPS
    GPS = models.CharField(null=True, max_length=15, default="")

    class Meta:
        db_table = "DonViGTVT_Curent"


class KhoaHoc(models.Model):
    Id = models.AutoField(primary_key=True)
    MonthID = models.IntegerField(null=True, default=1)
    KhoaHocID = models.CharField(null=True, max_length=15, default="")
    MaKH = models.CharField(null=True, max_length=13, default="")
    MaCSDT = models.CharField(null=True, max_length=6, default="")
    MaSoGTVT = models.CharField(null=True, max_length=6, default="")
    TenKH = models.CharField(null=True, max_length=50, default="")
    NgayKG = models.DateField(null=True, )

    class Meta:
        db_table = "KhoaHoc"

class KhoaHocDangDT(models.Model):
    MonthID = models.IntegerField(null=True, default=1)
    KhoaHocID = models.AutoField(primary_key=True)
    MaKH = models.CharField(null=True, max_length=13, default="")
    MaCSDT = models.CharField(null=True, max_length=6, default="")
    MaSoGTVT = models.CharField(null=True, max_length=6, default="")
    TenKH = models.CharField(null=True, max_length=50, default="")
    NgayKG = models.DateField(null=True, )

    class Meta:
        db_table = "KhoaHocDangDT"

class NguoiLX(models.Model):
    Id = models.AutoField(primary_key=True)
    KhoaHocID = models.IntegerField(null=True, default=0)
    GiaoVienID = models.CharField(null=True, max_length=15, default='0')
    NguoiLXID = models.CharField(null=True, max_length=15, default='')
    MaDK = models.CharField(null=True, max_length=25, default="")
    HoDemNLX = models.CharField(null=True, max_length=30, default="")
    TenNLX = models.CharField(null=True, max_length=20, default="")
    NgaySinh = models.DateField(null=True, )
    HangGPLX = models.CharField(null=True, max_length=50, default="")
    HangDaoTao = models.CharField(null=True, max_length=50, default="")
    SoCMT = models.CharField(null=True, max_length=20, default="")
    SizeImage = models.CharField(max_length=20, default='')

    class Meta:
        db_table = "NguoiLX"

class NguoiLXLocal(models.Model):
    Id = models.AutoField(primary_key=True)
    KhoaHocID = models.IntegerField(null=True, default=0)
    NguoiLXID = models.CharField(null=True, max_length=15, default='')
    MaDK = models.CharField(null=True, max_length=25, default="")
    HoVaTen = models.CharField(null=True, max_length=50, default="")
    NgaySinh = models.DateField(null=True, )
    HangGPLX = models.CharField(null=True, max_length=50, default="")
    HangDaoTao = models.CharField(null=True, max_length=50, default="")
    SoCMT = models.CharField(null=True, max_length=20, default="")
    SoDienThoai = models.CharField(null=True, max_length=15, default="")
    CreateDate = models.DateTimeField(null=True)
    IsExport = models.BooleanField(default=False)

    class Meta:
        db_table = "NguoiLXLocal"

class NguoiLXFingerLocal(models.Model):
    FingerID = models.AutoField(primary_key=True)
    KhoaHocID = models.IntegerField(null=True, default=0)
    NguoiLXID = models.IntegerField(null=True, default=0)
    ViTri = models.IntegerField(null=True, default=0)
    Templace = models.BinaryField(null=True)

    class Meta:
        db_table = "NguoiLXFingerLocal"


class NguoiLXFaceLocal(models.Model):
    PictureID = models.AutoField(primary_key=True)
    KhoaHocID = models.IntegerField(null=True, default=0)
    NguoiLXID = models.IntegerField(null=True, default=0)
    AddDated = models.DateTimeField(null=True, auto_now_add=True)
    Image = models.BinaryField(null=True, )
    LengthImage = models.IntegerField(null=True, default=0)

    class Meta:
        db_table = "NguoiLXFaceLocal"


class HangDT(models.Model):
    Id = models.AutoField(primary_key=True)
    MaHangDT = models.CharField(null=True, max_length=10, default="")
    TenHangDT = models.CharField(null=True, max_length=50, default="")
    HangGPLX = models.CharField(null=True, max_length=10, default="")
    ThoiGianDaoTao = models.FloatField(null=True, default=0)
    QuangDuongYC = models.FloatField(null=True, default=0)

    class Meta:
        db_table = "HangDT"


class GiaoVien(models.Model):
    Id = models.AutoField(primary_key=True)
    GiaoVienID = models.CharField(null=True, max_length=15, default='')
    MaGV = models.CharField(null=True, max_length=8, default="")
    HoTenDem = models.CharField(null=True, max_length=50, default="")
    TenGV = models.CharField(null=True, max_length=50, default="")
    NgaySinh = models.DateTimeField(null=True, )
    HangGPLX = models.CharField(null=True, max_length=10, default="")
    AnhChup = models.BinaryField(null=True, )
    SoCMT = models.CharField(null=True, max_length=20, default="")
    SizeImage = models.CharField(max_length=20, default='')

    class Meta:
        db_table = "GiaoVien"


class GiaoVienFinger(models.Model):
    FingerID = models.AutoField(primary_key=True)
    GiaoVienID = models.IntegerField(null=True, default=0)
    Templace = models.BinaryField(null=True, )
    CreateDate = models.DateTimeField(null=True, )
    ViTri = models.IntegerField(null=True, default=0)

    class Meta:
        db_table = "GiaoVienFinger"

class NguoiLXFinger(models.Model):
    FingerID = models.AutoField(primary_key=True)
    KhoaHocID = models.IntegerField(null=True, default=0)
    NguoiLXID = models.IntegerField(null=True, default=0)
    ViTri = models.IntegerField(null=True, default=0)
    Templace = models.BinaryField(null=True)

    class Meta:
        db_table = "NguoiLXFinger"


class NguoiLXFace(models.Model):
    PictureID = models.AutoField(primary_key=True)
    KhoaHocID = models.IntegerField(null=True, default=0)
    NguoiLXID = models.IntegerField(null=True, default=0)
    AddDated = models.DateTimeField(null=True, auto_now_add=True)
    Image = models.BinaryField(null=True, )
    LengthImage = models.IntegerField(null=True, default=0)

    class Meta:
        db_table = "NguoiLXFace"


class Vehicles(models.Model):
    Id = models.AutoField(primary_key=True)
    VehicleID = models.CharField(null=True, max_length=15, default='')
    MaCSDT = models.CharField(null=True, max_length=50)
    Imei = models.CharField(null=True, max_length=15)
    BienSoXe = models.CharField(null=True, max_length=20)
    HangGPLX = models.CharField(null=True, max_length=20, default='')
    ThoiHanSuDung = models.IntegerField(null=True, default=0)
    NgayBatDau = models.DateTimeField(null=True, default=datetime.now, blank=True)
    NgayKetThuc = models.DateTimeField(null=True, default=datetime.now, blank=True)
    TongThoiGianConLai = models.IntegerField(null=True, default=0)  # tính theo giờ
    Version = models.CharField(null=True, max_length=50, default='')
    HeSoXung = models.IntegerField(null=True, default=0)
    Active = models.IntegerField(null=True, default=0)

    class Meta:
        db_table = "Vehicles"


class HistoryData(models.Model):
    HistoryID = models.AutoField(primary_key=True)
    MaCSDT = models.CharField(null=True, max_length=6, default="")
    PhienDaoTao = models.IntegerField(null=True, default=0)
    VehicleID = models.IntegerField(null=True, default=0)
    Imei = models.CharField(null=True, max_length=50, default='')
    GiaoVienID = models.IntegerField(null=True, default=0)
    NguoiLXID = models.IntegerField(null=True, default=0)
    ThoiDiem = models.DateTimeField(null=True, auto_now_add=True)
    TongQuangDuongLD_Phien = models.FloatField(null=True, default=0)
    TongThoiGianLD_Phien = models.FloatField(null=True, default=0)
    TongQuangDuongLD = models.FloatField(null=True, default=0)
    TongThoiGianLD = models.FloatField(null=True, default=0)
    VanToc = models.FloatField(null=True, default=0)
    Lat = models.FloatField(null=True, default=0)
    Lng = models.FloatField(null=True, default=0)
    AnhChup = models.BinaryField(null=True, )
    FaceStatus = models.BooleanField(null=True, default=False)
    CameraStatus = models.IntegerField(null=True, default=0)
    RFIDStatus = models.IntegerField(null=True, default=0)
    FingerStatus = models.IntegerField(null=True, default=0)
    CreateDate = models.DateTimeField(null=True, auto_now_add=True)
    Version = models.CharField(null=True, max_length=10, default='1.0')
    NhietDo = models.FloatField(null=True,default=0)
    DienAp = models.FloatField(null=True, default=0)
    CpuInf = models.IntegerField(null=True, default=0)
    MemoryCard = models.CharField(null=True,max_length=20, default="")
    IsSync = models.BooleanField(default=False)

    class Meta:
        db_table = "HistoryData"


class DiemDanhNguoiLx(models.Model):
    Id = models.AutoField(primary_key=True)
    DiemDanhNguoiLxID = models.CharField(null=True, max_length=15,default='')
    PhienDaoTao = models.CharField(null=True, max_length=15,default='')
    GiaoVienID = models.IntegerField(null=True, default=0)
    NguoiLXID = models.IntegerField(null=True, default=0)
    ThoiDiem = models.DateTimeField(null=True, auto_now_add=True)
    LoaiDiemDanh = models.BooleanField(null=True, default=False)
    VehicleID = models.IntegerField(null=True, default=0)
    Lat = models.FloatField(null=True, default=0)
    Lng = models.FloatField(null=True, default=0)
    TongQuangDuongLD_Phien = models.FloatField(null=True, default=0)
    TongThoiGianLD_Phien = models.FloatField(null=True, default=0)
    TongQuangDuong = models.FloatField(null=True, default=0)
    TongThoiGian = models.FloatField(null=True, default=0)
    AnhChup = models.BinaryField(null=True, )
    CreateDate = models.DateTimeField(null=True, auto_now_add=True)
    IsSync = models.BooleanField(default=False)

    class Meta:
        db_table = "DiemDanhNguoiLx"


class DiemDanhGiaoVien(models.Model):
    Id = models.AutoField(primary_key=True)
    GiaoVienDiemDanhID = models.CharField(null=True, max_length=15,default='')
    GiaoVienID = models.IntegerField(null=True, default=0)
    ThoiDiem = models.DateTimeField(null=True, auto_now_add=True)
    LoaiDiemDanh = models.BooleanField(null=True, default=False)
    VihicleID = models.IntegerField(null=True, default=0)
    Lat = models.FloatField(null=True, default=0)
    Lng = models.FloatField(null=True, default=0)
    IsSync = models.BooleanField(default=False)

    class Meta:
        db_table = "DiemDanhGiaoVien"

class ThongTinThietBi(models.Model):
    ThietBiID = models.AutoField(primary_key=True)
    # Data Finger
    DeviceFingerSerial = models.CharField(null=True, max_length=15, default='/dev/ttyUSB0')
    DeviceFingerBaudrate  = models.IntegerField(null=True, default=115200)
    DeviceFingerStatus = models.BooleanField(null=True, default=False)
    # Data RFID
    # DeviceRFIDType = models.IntegerField(null=True, default=0)
    DeviceRFIDSerial = models.CharField(null=True, max_length=15, default='/dev/ttyUSB1')
    DeviceRFIDBaudrate  = models.IntegerField(null=True, default=19200)
    DeviceRFIDStatus = models.BooleanField(null=True, default=False)
    # DataGPS
    DeviceGPSSerial = models.CharField(null=True, max_length=15, default='/dev/serial0')
    DeviceGPSBaudrate  = models.IntegerField(null=True, default=9600)
    DeviceGPSStatus = models.BooleanField(null=True, default=False)
    # DataPulse
    DeviceGPIOPort = models.IntegerField(null=True, default=24)
    DeviceGPSStatus = models.BooleanField(null=True, default=False)
    # Data Internet status
    DeviceInternetStatus = models.BooleanField(null=True, default=True)
    # DataRaspberryPi %
    DeviceMemory = models.IntegerField(null=True, default=0)
    DeviceCPU = models.IntegerField(null=True, default=0)
    DeviceRam = models.IntegerField(null=True, default=0)
    # Data camera
    DeviceCameraStatus = models.BooleanField(null=True, default=False)
    # Status Sever
    ServerStatus = models.BooleanField(null=True, default=False)

    class Meta:
        db_table = "ThongTinThietBi"


class VerhicleHistory(models.Model):
    id = models.AutoField(primary_key=True)
    MaCSDT = models.CharField(null=True, max_length=30, default="")
    VehicleID = models.IntegerField(null=True, default=0)
    AddDated = models.DateTimeField(null=True, auto_now_add=True)
    VanToc = models.FloatField(null=True, default=0)
    Lat = models.FloatField(null=True, default=0)
    Lng = models.FloatField(null=True, default=0)
    CreateDate = models.DateTimeField(null=True, auto_now_add=True)
    IsSync = models.BooleanField(default=False)

    class Meta:
        db_table = "VerhicleHistory"


class LogSystem(models.Model):
    id = models.AutoField(primary_key=True)
    file_name = models.CharField(null=True, default='', max_length=100)
    method_name =  models.CharField(null=True, default='', max_length=100)
    error_content = models.CharField(null=True, default='', max_length=500)
    log_type = models.CharField(null=True, default='error', max_length=50)
    create_date = models.DateTimeField(null=True, default=datetime.now(), blank=True)

    class Meta:
        db_table = "LogSystem"

def write_log(file_name, method_name, error_content, log_type = 'error'):
    log_system = LogSystem()
    log_system.file_name = file_name
    log_system.method_name = method_name
    log_system.error_content = error_content
    log_system.log_type = log_type
    log_system.save()