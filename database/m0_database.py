import os, sys
sys.path.append(os.getcwd())

# Turn off bytecode generation
# https://docs.djangoproject.com/en/3.0/topics/db/models/
import datetime
import sys
sys.dont_write_bytecode = True
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")
import django
django.setup()

# for u in NguoiLX.objects.all():
# 	print("\tPictureID: " + str(u.NguoiLXID))
# 	print("\tNguoiLxID: " + str(u.HoDemNLX))


# Import your models for use in your script
from database.models import *

# save data to GiaoVien
# giao_vien = GiaoVien(HoTenDem = 'Le Hong',TenGV = 'Tuan', NgaySinh = datetime.date(1993, 9, 16),HangGPLX = 'A3')
# giao_vien.CreateDate = datetime.date.today()
# giao_vien.save()

# fillter
# giao_vien = GiaoVien.objects.filter(HangGPLX='A2',TenGV='Trung').order_by('-CreateDate')
# for u in giao_vien:
# 	print("GiaoVienID: " + str(u.GiaoVienID) + "\tTenGV: " + u.TenGV + "\tCreateDate: " + str(u.CreateDate))

# update
# giao_vien = GiaoVien.objects.get(HangGPLX='A2',TenGV='Trung')
# giao_vien.TenGV = 'Trung 1'
# giao_vien.save()

# donvi = DonViGTVT_Curent()
# donvi.MaDV = 'izi'
# donvi.TenDV = 'Công ty IZI Software'
# donvi.save()

# gvfinger = GiaoVienFinger.objects.all()
# gvfinger[0].delete()
# print('done',len(gvfinger))

# nlxfinger = NguoiLXFinger.objects.all()
# nlxfinger[0].delete()
# print('done',len(nlxfinger))

# gvfinger = GiaoVienFinger()
# gvfinger.GiaoVienID = '1'
# gvfinger.Templace = b'trung bỏ vân tay dạng binary vào đây'
# gvfinger.CreateDate = '2020-06-05'
# gvfinger.save()

# kh = KhoaHoc.objects.all()[0]
# kh.MaKH = 'ma kh'
# kh.MaSoGTVT = 'MSGTVT'
# kh.MaCSDT = 'izi'
# kh.TenKH = 'Khóa Học Lái Xe OTO'
# kh.NgayKG = '2020-06-01'
# kh.save()

# nglx = NguoiLX.objects.get(NguoiLXID = 5)
# nglx.KhoaHocID = 1
# nglx.MaDK = 'ma dk'
# nglx.HoDemNLX = 'Lê Hồng'
# nglx.TenNLX = 'Phương'
# nglx.NgaySinh = '1993-07-09'
# nglx.HangGPLX = 'B2'
# nglx.HangDaoTao = 'Hạng B1 lên B2'
# nglx.save()
# # NguoiLXID = 1

# hang_dt = HangDT()
# hang_dt.MaHangDT = 'B2'
# hang_dt.TenHangDT = 'Hạng B2'
# hang_dt.HangGPLX = 'B2'
# hang_dt.ThoiGianDaoTao = '36'
# hang_dt.QuangDuongYC = '810'
# hang_dt.save()

# nglx = NguoiLX()
# nglx.NguoiLXID = 5
# nglx.KhoaHocID = 1
# nglx.MaDK = 'ma dk'
# nglx.HoDemNLX = 'Lê Võ Hồng'
# nglx.TenNLX = 'Phương'
# nglx.NgaySinh = '1993-07-09'
# nglx.HangGPLX = 'A1'
# nglx.HangDaoTao = 'Hạng đào tạo'
# nglx.save()

# nlx = NguoiLX.objects.get(NguoiLXID = 5)
# # nlx = NguoiLX()
# nlx.SoCMT = '239475943'
# nlx.save()


# gv = GiaoVien()
# gv.MaGV = 'MaGV1'
# gv.HoTenDem = 'Lê Minh'
# gv.TenGV = 'Phương'
# gv.NgaySinh = '1993-07-09'
# gv.HangGPLX = 'A2'
# gv.AnhChup = b''
# gv.save()

# gv = GiaoVien.objects.get(MaGV = 'MaGV1')
# # gv = GiaoVien()
# gv.SoCMT = '212793690'
# gv.save()


# from modules.m11_rasbery_pi import get_serial
# v = Vehicles.objects.get(BienSoXe = '76F1 12398')
# v.Imei = '10000000cb3dd4b7'
# v.BienSoXe = '76F1 12398'
# v.HangGPLX = 'A1'
# v.save()

# h = HistoryDataTemp()
# h.FaceStatus = False
# h.CameraStatus = False
# h.RFIDStatus = False
# h.FingerStatus = False
# h.Version = '1.0'
# h.save()

# select all
# print('select all')
# for u in NguoiLX.objects.all():
# 	print("\tPictureID: " + str(u.NguoiLXID))
# 	print("\tNguoiLxID: " + str(u.HoDemNLX))

# print(HistoryDataTemp.objects.all()[0].Version)

