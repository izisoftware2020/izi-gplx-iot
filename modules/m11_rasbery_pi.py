from __future__ import division
import glob
import serial
import time
from subprocess import PIPE, Popen
import psutil
import urllib.request
import socket

rfidType = 1
from config.config import *

def get_cpu_temperature():
	process = Popen(['vcgencmd', 'measure_temp'], stdout=PIPE)
	output, _error = process.communicate()
	# print("temps" , output)
	return (output)

def get_cpu_usage():
	p = psutil.Process()
	cpu_usage = p.cpu_percent(interval=1)
	# print ("cpu",cpu_usage)
	return cpu_usage
	
def get_disk_usage():
	disk = psutil.disk_usage('/')
	disk_total = disk.total / 2**30     # GiB.
	disk_used = disk.used / 2**30
	disk_free = disk.free / 2**30
	disk_percent_used = disk.percent
	return disk_total, disk_used, disk_free

def get_serial():
	# Extract serial from cpuinfo file
	cpuserial = "0000000000000000"
	try:
		f = open('/proc/cpuinfo', 'r')
		for line in f:
			if line[0:6] == 'Serial':
				cpuserial = line[10:26]
		f.close()
	except:
		cpuserial = "ERROR000000000"
	return cpuserial

def DiscoveryFinger(deviceDB):
	dirlist = glob.glob("/dev/*USB*")
	Baudrates = [9600, 19200, 38400, 57600, 115200]
	print("Start Discovery Finger")
	for serialDevice in dirlist:
		for Baudrate in Baudrates:
			time.sleep(0.5)
			# Now try
			try:
				print ('..', end="", flush=True)
				ser = serial.Serial(serialDevice, Baudrate)
				#Send CMD
				message = [0xEF, 0x01, 0xFF, 0xFF, 0xFF, 0xFF, 0x01, 0x00, 0x03, 0x1d, 0x00, 0x21]
				ser.write(message)
				time.sleep(0.5)
				data = ser.read(ser.inWaiting())
				if len(data) > 1:
					if data[0] == 0xEF and data[1] == 0x01:
						print("Found!!!")
						print("DeviceFinger", serialDevice, "Baudrate" , Baudrate )
						#Save serialDevice and Baudrate to DB
						deviceDB.DeviceFingerSerial = serialDevice
						deviceDB.DeviceFingerBaudrate = Baudrate
						deviceDB.DeviceFingerStatus = True
						deviceDB.save()
						return True
			except:
				pass

	deviceDB.DeviceFingerStatus = False
	deviceDB.save()
	return False

def DiscoveryRFID(deviceDB):
	# rfidType = 1
	# from config.config import *
	if rfidType == 2:
		print("Start Discovery RFID")
		return True
	elif rfidType == 1:
		dirlist = glob.glob("/dev/*USB*")
		Baudrates = [9600, 19200, 38400, 57600,115200]
		print("Start Discovery RFID")
		for serialDevice in dirlist:
			for Baudrate in Baudrates:
				time.sleep(0.5)
				#Now try
				try:
					print ('..', end="", flush=True)
					ser = serial.Serial(serialDevice, Baudrate)
					#Send CMD
					message = [ 0xaa, 0xbb, 0x06, 0x00, 0x00, 0x00, 0x06, 0x01, 0x64, 0x63]
					ser.write(message)
					time.sleep(0.5)
					data = ser.read(ser.inWaiting())
					if len(data) > 1 :
						if data[0] == 0xaa and data[1] == 0xbb :
							print("Found!!!")
							print("DeviceRFID", serialDevice, "Baudrate", Baudrate )
							#Save serialDevice and Baudrate to DB
							deviceDB.DeviceRFIDSerial = serialDevice
							deviceDB.DeviceRFIDBaudrate = Baudrate
							deviceDB.DeviceRFIDStatus = True
							deviceDB.save()
							return True
				except:
					pass
		deviceDB.DeviceRFIDStatus = False
		deviceDB.save()
		return False

def DiscoveryGPS(deviceDB):
	dirlist = ['/dev/serial0']
	print (dirlist)
	Baudrates = [9600 , 19200 , 38400 , 57600 ,115200]
	print("Start Discovery GPS")
	for serialDevice in dirlist:
		for Baudrate in Baudrates:
			time.sleep(0.5)
			#Now try
			try:
				print ('..' , end="", flush=True)
				#Send CMD
				ser = serial.Serial(serialDevice, Baudrate)
				data = '$PMTK314,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*29\r\n'
				data_send = bytearray(data.encode())
				ser.write(data_send)
				time.sleep(2)
				data_receive = ser.read(ser.inWaiting())
				if len(data_receive) > 1:
					print("Found!!!")
					print("DeviceGPS", serialDevice, "Baudrate" , Baudrate )
					#Save serialDevice and Baudrate to DB
					deviceDB.DeviceGPSSerial = serialDevice
					deviceDB.DeviceGPSBaudrate = Baudrate
					deviceDB.DeviceGPSStatus = True
					deviceDB.save()
					return True
			except:
				pass
	deviceDB.DeviceGPSStatus = False
	deviceDB.save()
	return False

def DiscoveryInternet(deviceDB):
	kn = False
	try:
		host = socket.gethostbyname('google.com')
		s = socket.create_connection((host, 80), 2)
		s.close()
		kn = True
	except:
		pass
	deviceDB.DeviceInternetStatus = kn
	deviceDB.save()

def DiscoveryServer(deviceDB, host=None):
	DiscoveryInternet(deviceDB)
	from service.s1_sync_data_from_server import check_connect_server
	deviceDB.ServerStatus = check_connect_server(host)
	deviceDB.save()