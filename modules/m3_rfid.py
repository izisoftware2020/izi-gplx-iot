import os

import serial
import time
import sys
import RPi.GPIO as GPIO
import mfrc522

# Turn off bytecode generation
# https://docs.djangoproject.com/en/3.0/topics/db/models/
import datetime
sys.dont_write_bytecode = True
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")
import django
django.setup()

# Import your models for use in your script
from database.models import *
rfidType = 1
from config.config import *


# header = [0xaa, 0xbb]
# address = [0x00, 0x00]


def print_array(data):
	print("Array Start")
	for i in range(len(data)):
		print(i, hex(data[i]))
	print("Array End")
	
	
def calculate_checksum(data):
	checksum = 0
	for i in range(4, len(data) - 1):
		checksum ^= data[i]
	return checksum
		
		
class RFID_CR522U:
	# CR522 reader
	InitializePort = 0x0101
	SetDeviceNodeNumber = 0x0102
	ReadDeviceNodeNumber = 0x0103
	ReadDeviceMode = 0x0104
	SetBuzzerBeep = 0x0106
	SetLedcolor = 0x0107
	SetReaderWorkingStatus = 0x0108
	SetAntennaStatus = 0x010c
	MifareRequest = 0x0201
	MifareAnticollision = 0x0202
	MifareSelect = 0x0203
	MifareHlta = 0x0204
	MifareAuthentication1 = 0x0206
	MifareAuthentication2 = 0x0207
	MifareRead = 0x0208
	MifareWrite = 0x0209
	MifareInitval = 0x020A
	MifareReadBalance = 0x020B
	MifareDecrement = 0x020C
	MifareIncrement = 0x020D
	MifareRestore = 0x020E
	MifareTransfer = 0x020F
	MifareUltraLightAnticoll = 0x0212
	MifareUltraLightWrite = 0x0213

	# RC522 Board
	PCD_IDLE = 0x00  # NO action; cancel current command
	PCD_AUTHENT = 0x0E  # verify key
	PCD_RECEIVE = 0x08  # receive data

	PCD_TRANSMIT = 0x04  # send data
	PCD_TRANSCEIVE = 0x0C  # receive and send data
	PCD_RESETPHASE = 0x0F  # reset
	PCD_CALCCRC = 0x03  # CRC calculation

	# Mifare_One Card command word
	PICC_REQIDL = 0x26  # line-tracking area is dormant  PICC_REQALL = 0x52      //line-tracking area is interfered
	PICC_REQIDL = 0x26  # line-tracking area is dormant  PICC_REQALL = 0x52      //line-tracking area is interfered
	PICC_ANTICOLL = 0x93  # Anti collision
	PICC_SElECTTAG = 0x93  # choose cards
	PICC_AUTHENT1A = 0x60  # Verify A key
	PICC_AUTHENT1B = 0x61  # Verify B key
	PICC_READ = 0x30  # Reader Module
	PICC_WRITE = 0xA0  # letter block

	PICC_DECREMENT = 0xC0
	PICC_INCREMENT = 0xC1
	PICC_RESTORE = 0xC2  # Transfer data to buffer
	PICC_TRANSFER = 0xB0  # Save buffer data
	PICC_HALT = 0x50  # Dormancy

	# MF522	Error	code	returned	when	communication
	MI_OK = 0
	MI_NOTAGERR = 1
	MI_ERR = 2


	def __init__ (self, dev='/dev/ttyUSB1'):
		if rfidType == 2:
			self.MIFAREReader = mfrc522.MFRC522()
			print('created MFRC522')
		else:
			info_device = ThongTinThietBi.objects.first()
			dev = info_device.DeviceRFIDSerial
			self.ser = serial.Serial(dev, 19200, timeout=0.1)

		self.RFID_DETECT = 0
		self.RFID_ID = bytearray([0x00, 0x00, 0x00, 0x00])

	def SetBuzzerBeep (self):
		if rfidType == 1:
			# value[8]*10ms =  time delay
			value = bytearray([0xaa, 0xbb, 0x06, 0x00, 0x00, 0x00, 0x06, 0x01, 0x64, 0x63])
			value[len(value) - 1] = calculate_checksum(value)
			# print_array( value)
			self.ser.write(value)
			s = self.ser.read(100)
	
	def SetLedcolor (self, mode):
		if rfidType == 1:
			# 0 = LED_RED Off , LED_GREEN Off
			# 1 = LED_RED On , LED_GREEN = Off
			# 2 = LED_GREEN Off , LED_RED On
			# 3 = LED_GREEN On , LED_RED ON
			value = bytearray([0xaa, 0xbb, 0x06, 0x00, 0x00, 0x00, 0x07, 0x01, 0x00, 0x05])
			value[8] = mode
			value[len(value) - 1] = calculate_checksum(value)
			# print_array( value)
			# self.ser.write(value)
		
	def SetAntennaStatus (self, mode):
		if rfidType == 1:
			# Set Anttenna Status which mode in
			# 0 = Close Field
			# 1 = Open Field
			value = bytearray([0xaa, 0xbb, 0x06, 0x00, 0x00, 0x00, 0x0c, 0x01, 0x00, 0x0d])
			value[8] = mode
			value[len(value) - 1] = calculate_checksum(value)
			# print_array( value)
			# self.ser.write(value)

	def MifareRequest (self, mode):
		if rfidType == 1:
			# Set Anttenna Status which mode in
			# 0x52 = All type card
			# 0x26 = Idle card
			value = bytearray([0xaa, 0xbb, 0x06, 0x00, 0x00, 0x00, 0x01, 0x02, 0x52, 0x51])
			value[8] = mode
			value[len(value) - 1] = calculate_checksum(value)
			# print_array( value)
			self.ser.write(value)
			s = self.ser.read(100)
			# print (s)
			return 0, 0
		elif rfidType == 2:
			status, TagType = self.MIFAREReader.MFRC522_Request(mode)
			# if status != self.MI_OK:
			# print("Card detected")
			# print('Mifare Request type 2: ', status, TagType)
			return status, TagType


	def MifareAnticollision (self):
		if rfidType == 1:
			# Set Anttenna Status which mode in
			# 0x52 = All type card
			# 0x26 = Idle card
			value = bytearray([0xaa, 0xbb, 0x05, 0x00, 0x00, 0x00, 0x02, 0x02, 0x00])
			value[len(value) - 1] = calculate_checksum(value)
			# print_array( value)
			self.ser.write(value)
			s = self.ser.read(100)
			self.RFID_ID = bytearray([0x00, 0x00, 0x00, 0x00])
			self.RFID_DETECT = 0
			if s[2] == 0x0a:
				self.RFID_ID[0:4] = s[9:13]
				self.RFID_DETECT = 1
			return self.RFID_DETECT, self.RFID_ID
		elif rfidType == 2:
			self.RFID_DETECT, self.RFID_ID = self.MIFAREReader.MFRC522_Anticoll()
			return self.RFID_DETECT, self.RFID_ID

	def MifareSelect (self):
		if rfidType == 1:
			value = bytearray([0xaa, 0xbb, 0x09, 0x00, 0x00, 0x00, 0x03, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00])
			value[8:12] = self.RFID_ID[0:4]
			value[len(value) - 1] = calculate_checksum(value)
			self.ser.write(value)
			time.sleep(0.1)
			s = self.ser.read(20)
		elif rfidType == 2:
			if len(self.RFID_ID) >= 4:
				self.MIFAREReader.MFRC522_SelectTag(self.RFID_ID)
			else:
				print('Mifare Select is null ID')



	def MifareAuthentication (self, auth_mode, block):
		if rfidType == 1:
			# aa bb xx 00 00 00 07 02 Auth_mode Block xx xx xx xx xx xx XOR
			value = bytearray([0xaa, 0xbb, 0x0d, 0x00, 0x00, 0x00, 0x07, 0x02, 0x60, 0x04, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x61])
			value[8] = auth_mode
			value[9] = block
			value[len(value) - 1] = calculate_checksum(value)
			# print("Wait for Authentication")
			# print_array(value)
			self.ser.write(value)
			time.sleep(0.1)
			s = self.ser.read(20)
			return self.MI_OK
		elif rfidType == 2:
			key = [0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF]
			return self.MIFAREReader.MFRC522_Auth(auth_mode, block, key, self.RFID_ID)


	def BlockMifareRead (self, block):
		if rfidType == 1:
			# aa bb 06 00 00 00 08 02 04 0e
			value = bytearray([0xaa, 0xbb, 0x06, 0x00, 0x00, 0x00, 0x08, 0x02, 0x04, 0x0e])
			value[8] = block
			value[len(value) - 1] = calculate_checksum(value)
			self.ser.write(value)
			time.sleep(0.05)
			s = self.ser.read(30)
			# need verify status
			return s[9:]
		elif rfidType == 2:
			s = self.MIFAREReader.MFRC522_Read(block)
			if s is not None:
				return s
			else:
				return ''

	def Read_DataBlock45(self):
		data1 = data2 = ''
		if self.MifareAuthentication(self.PICC_AUTHENT1A, 4) == self.MI_OK:
			data1 = self.BlockMifareRead(4)
			# if self.MifareAuthentication(self.PICC_AUTHENT1A, 5):
			data2 = self.BlockMifareRead(5)
		return data1 + data2



	def convert(self, s):
		string_ints = [chr(int) for int in s]
		return ''.join(string_ints)

	def ParseData (self, dataArray):
		status = False
		if rfidType == 1:
			datastring = dataArray[:16].decode("utf-8")
			x = datastring.split(".")
			if len(x) == 3:
				status = True
				# print("valid Data")
			else:
				status = False
				print("Invalid Format Data")
			return status, x
		elif rfidType == 2:
			datastring = self.convert(dataArray)
			x = datastring.split(".")
			if len(x) == 3:
				status = True
			else:
				print("Invalid Format Data")
			return status, x


	def LoginRFID(self):
		if rfidType == 1:
			# print(datetime.now().strftime("%H:%M:%S %f"), 'start Login RFID')
			self.MifareRequest(0x52)
			start = time.time()
			now = start
			self.SetLedcolor(3)
			while (now - start) < 0.1:  # trong 100ms neu khong co tay thi out
				time.sleep(0.01)
				now = time.time()
				self.MifareRequest(0x52)
				detect, self.RFID_ID = self.MifareAnticollision()
				if detect:
					cardstr = '{0:0{1}X}'.format(self.RFID_ID[0], 2) + "-" + '{0:0{1}X}'.format(self.RFID_ID[1], 2) + "-" + '{0:0{1}X}'.format(self.RFID_ID[2], 2) + "-" + '{0:0{1}X}'.format(self.RFID_ID[3], 2)
					print('Detect card:' + cardstr)
					self.MifareSelect()
					data = self.Read_DataBlock45()
					# print('phuong11', data)
					status, value = self.ParseData(data)
					if status:
						self.SetBuzzerBeep()
						# self.SetLedcolor(0)
						# self.SetLedcolor(3)
						# self.SetLedcolor(2)
						# self.SetLedcolor(1)
						return True, True, value[0], value[1], value[2]
					else:
						print ("No valid Data")
				else:
					return False, False, None, None, None
			self.SetLedcolor(3)
			return True, False, None, None, None
		elif rfidType == 2:
			status, TagType = self.MifareRequest(self.PICC_REQIDL)
			if status != self.MI_OK:
				return False, False, None, None, None
			start = time.time()
			now = start
			while (now - start) < 0.1:  # trong 20s neu khong co tay thi out
				now = time.time()
				detect, self.RFID_ID = self.MifareAnticollision()
				time.sleep(0.05)
				if detect == self.MI_OK:
					cardstr = '{0:0{1}X}'.format(self.RFID_ID[0], 2) + "-" + '{0:0{1}X}'.format(self.RFID_ID[1], 2) + "-" + '{0:0{1}X}'.format(self.RFID_ID[2], 2) + "-" + '{0:0{1}X}'.format(self.RFID_ID[3], 2)
					print('Detect card:' + cardstr)
					self.MifareSelect()
					data = self.Read_DataBlock45()
					status, value = self.ParseData(data)
					if status:
						return True, True, value[0], value[1], value[2]

					else:
						print("No valid Data")
				else:
					return False, False, None, None, None
			return True, False, None, None, None


# new.ReadMifare()
# new.SetBuzzerBeep()
		
# new = RFID_CR522U()
# new.LoginRFID()

# new.SetBuzzerBeep()
# new.SetLedcolor(3)
# new.SetAntennaStatus(0)
# new.MifareRequest(0x52)
# detect , id  = new.MifareAnticollision()
# print ("detect card id ", id[0] , id[1] , id[2] , id[3])
# print(new.RFID_ID)
# print("select mifare")
# new.MifareSelect()
# new.MifareAuthentication(0x60, 0x05)
# new.MifareRead(0x05)
# new.ReadMifare()
# new.SetBuzzerBeep()
"""
while 1:
	time.sleep(3)
	new.MifareRequest(0x52)
	detect , id  = new.MifareAnticollision()
	if detect == 1 :
		print ("detect card id ", id[0] , id[1] , id[2] , id[3])
	else :
		print ("no detect card")
"""

