import os, sys

import sys
sys.dont_write_bytecode = True
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")
import django
django.setup()

# Import your models for use in your script

import RPi.GPIO as GPIO


class COMMON:
    _instance = None
    SELECT_USB = 18
    ALLOW_LAN = 21
    ALLOW_SPEAKER = 17
    ALLOW_RC522_BROAD = 25

    @classmethod
    def instance(cls):
        if cls._instance is None:
            print('Creating new COMMON instance')
            cls._instance = cls.__new__(cls)

            GPIO.setwarnings(False)
            GPIO.setmode(GPIO.BCM)

            GPIO.setup(cls.SELECT_USB, GPIO.OUT)
            GPIO.setup(cls.ALLOW_LAN, GPIO.OUT)
            GPIO.setup(cls.ALLOW_SPEAKER, GPIO.OUT)
            GPIO.setup(cls.ALLOW_RC522_BROAD, GPIO.OUT)

        return cls._instance

    def enableSpeaker(self, speaked):
        if speaked:
            GPIO.output(self.ALLOW_SPEAKER, GPIO.LOW)
        else:
            GPIO.output(self.ALLOW_SPEAKER, GPIO.HIGH)
        print('Enable Speaker: ', speaked)

    def enableEthernet(self, Connnected):
        if Connnected:
            GPIO.output(self.ALLOW_LAN, GPIO.LOW)
        else:
            GPIO.output(self.ALLOW_LAN, GPIO.HIGH)
        print('Enable Ethernet: ', Connnected)

    def setUsbRfidEnable(self):
        GPIO.output(self.SELECT_USB, GPIO.LOW)
        print('Select using Port 0 - RFID')

    def setUsbFingerEnable(self):
        GPIO.output(self.SELECT_USB, GPIO.HIGH)
        print('Select using Port 1 - Fingerprint')

    def AllowRC522broad(self):
        GPIO.output(self.ALLOW_RC522_BROAD, GPIO.HIGH)
        print('Allow RC522 broad')
