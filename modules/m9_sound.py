import os, sys
import pygame

from modules.m0_common import COMMON

sys.path.append(os.getcwd())
pygame.mixer.init()

PATH = 'media/sounds/'

def play_so_tien_khong_du():
    common = COMMON.instance()
    common.enableSpeaker(True)
    FileName = 'so_tien_khong_du'
    pygame.mixer.music.load(PATH + FileName + '.mp3')
    print('Play Media: ' + FileName)
    pygame.mixer.music.play()
    while pygame.mixer.music.get_busy() == True:
        continue
    common.enableSpeaker(False)

def play_quet_the_thanh_cong():
    common = COMMON.instance()
    common.enableSpeaker(True)
    FileName = 'quet_the_thanh_cong'
    pygame.mixer.music.load(PATH + FileName + '.mp3')
    print('Play Media: ' + FileName)
    pygame.mixer.music.play()
    while pygame.mixer.music.get_busy() == True:
        continue
    common.enableSpeaker(False)

def play_quet_the_that_bai():
    common = COMMON.instance()
    common.enableSpeaker(True)
    FileName = 'quet_the_that_bai'
    pygame.mixer.music.load(PATH + FileName + '.mp3')
    print('Play Media: ' + FileName)
    pygame.mixer.music.play()
    while pygame.mixer.music.get_busy() == True:
        continue
    common.enableSpeaker(False)
