"""izi_iot URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
 

# import view
from apps.a1_login.views import A1LoginView
from apps.a2_dashboard.views import IndexView

urlpatterns = [
    path('admin/', admin.site.urls),

    # a1_login
    path('a1_login', IndexView.as_view(), name='a1_login'),

    # a2_dashboard
    path('a2_dashboard', IndexView.as_view(), name='a2_dashboard'),

    # a3_status
    path('a3_status', IndexView.as_view(), name='a3_status'),

    # a3_logout
    path('a3_logout', IndexView.as_view(), name='a3_logout'),

    # a4_speed
    path('a4_speed', IndexView.as_view(), name='a4_speed'),

    # a5_sync_server_local
    path('a5_sync_server_local', IndexView.as_view(),
         name='a5_sync_server_local'),

    # a6_setting
    path('a6_setting', IndexView.as_view(), name='a6_setting'),

    # a7_search
    path('a7_search', IndexView.as_view(), name='a7_search'),

    # a8_add_student
    path('a8_add_student', IndexView.as_view(), name='a8_add_student'),

    # a9_warning
    path('a9_warning', IndexView.as_view(), name='a9_warning'),
]


from django.conf import settings
from django.conf.urls.static import static

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)