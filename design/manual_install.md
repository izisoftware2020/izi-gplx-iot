# HƯỚNG DẪN CÀI ĐẶT

# Setup raspian OS for raspberry pi [guide](https://raspberrypi.vn/thu-thuat-raspberry-pi/huong-dan-cai-dieu-hanh-cho-raspberry-pi-2457.pi)
```
- Download Raspian: https://downloads.raspberrypi.org/raspbian_latest
- Win32DiskImager : https://sourceforge.net/projects/win32diskimager/
- Note for MACOS: balenaEtcher https://www.balena.io/etcher/
```

# Setup OpenCV Lib
- lib dependence for opencv: https://pimylifeup.com/raspberry-pi-opencv/
```
sudo apt update
sudo apt upgrade
sudo apt install cmake build-essential pkg-config git
sudo apt install libjpeg-dev libtiff-dev libjasper-dev libpng-dev libwebp-dev libopenexr-dev
sudo apt install libavcodec-dev libavformat-dev libswscale-dev libv4l-dev libxvidcore-dev libx264-dev libdc1394-22-dev libgstreamer-plugins-base1.0-dev libgstreamer1.0-dev
sudo apt install libgtk-3-dev libqtgui4 libqtwebkit4 libqt4-test python3-pyqt5
sudo apt install libatlas-base-dev liblapacke-dev gfortran
sudo apt install libhdf5-dev libhdf5-103

sudo apt install python3-dev python3-pip python3-numpy
sudo apt-get install qt5-default pyqt5-dev pyqt5-dev-tools
```
## Install face_recognition
```
pip3 install opencv-contrib-python==4.1.0.25
pip3 install dlib face_recognition imutils
pip3 install xmltodict apscheduler
```
```
pip3 install django
```

# pyuic5 để convert ui file to Py file
```
pyuic5 -x setting.ui -o setting.py
```

# Setup IPCamera
```
rtsp://admin:admin@123456@192.168.0.10:554/cam/realmonitor?channel=1&subtype=0


sudo ifconfig eth0 192.168.0.99 netmask 255.255.0.0
sudo ip route add 192.168.0.0/24 via 192.168.0.99

```

# Setup DCOM
``` 
# install lib 
sudo apt-get install ppp usb-modeswitch wvdial
# config on file /etc/wvdial.conf
sudo nano /etc/wvdial.conf
===================================
[Dialer Defaults]
Init1 = ATZ
Init2 = ATQ0 V1 E1 S0=0
Modem Type = Analog Modem
ISDN = 0
New PPPD = yes
Phone = *99#
Modem = /dev/ttyUSB0
Username = { }
Password = { }
Baud = 9600
===================================
# Enable config 
sudo wvdialconf /etc/wvdial.conf
# runing
sudo wvdial
```
# Keybroad install
```
sudo apt install matchbox-keyboard
```

====================================

# setup comport (serial port)
```
sudo raspi-config       # => Interfacing Options => Serial => No => Yes
                             (Disable Login shell - Enable serial interface)
pip3 install pyserial   # if need
```
====================================
# Hướng dẫn cài đặt teamviewer teamviewer
```
wget https://download.teamviewer.com/download/linux/teamviewer-host_armhf.deb
sudo dpkg -i teamviewer-host_armhf.deb
sudo apt --fix-broken install
```

# SetUp GPS kết nối thiết bị giả lập
```
3: GND
4: RX
5: TX
9: XUNG
```
# Kết nối phần cứng
```
- (06) GND              Ground
- (08) RX (GPS)         TXD0-UART
- (10) TX (GPS)         RXD0-UART
- (18) Tín hiệu Xung:   GPIO24 (sườn lên)
- (12) Select USB:      GPIO18
- (11) Allow Speacker:  GPIO17 (LOW to active)
- (40) Allow LAN:       GPIO21  (LOW to active)
https://www.element14.com/community/servlet/JiveServlet/showImage/102-92640-8-726998/GPIO-Pi4.png
```

# Format RFID card string
```
KhoaHocID.NguoiLxID.SoCMT <=> 0001.000002.123456789
GIAOVIEN.GiaoVienID.SoCMT 
```

# Sử dụng RC522 broad để đọc RFID
Xem chi tiết tại đây: https://pimylifeup.com/raspberry-pi-rfid-rc522/
```
- Kết nối RC522 với Raspberry Pi 4 (theo hình bên trên)
- sudo raspi-config : để enable spi  
- install library
sudo pip3 install spidev
sudo pip3 install mfrc522
```
![img_3.png](img_3.png)
![img_2.png](img_2.png)


# Hướng dẫn sử dụng Bumpversion
```
- Version of DAT device : {major}.{minor}.{patch}
- bumpversion --allow-dirty patch
- Khi muốn tăng  major hoặc minor thì phải edit bằng tay (manual)
ở file: .bumpversion.cfg
và 3 file: buil_release.py, buil_release_window.py, a3_status.py
```

# Hướng dẫn cài đặt tự động khởi động khi reboot thiết bị 

- Copy toàn bộ các file trong thư mục `deploy` vào trong thư mục `/home/pi/izi-gplx-iot`:
- Config crontab cho chạy tự động
   1. open crontab: `crontab -e`
   2. Thêm đoạn mã để tự động chạy khi khởi động Raspberry Pi :
```
  # @reboot sleep 5 && sudo sh /home/pi/izi-gplx-iot/start_dcom.sh > /home/pi/izi-gplx-iot/Logs/start_log.txt
  @reboot sleep 10 && DISPLAY=:0 /home/pi/izi-gplx-iot/main.sh start >> /home/pi/izi-gplx-iot/dat_Logs/$(date --utc +\%Y\%m\%d).log 
  @reboot python3 /home/pi/izi-gplx-iot/main.py &
```
@reboot sleep 10 && sudo sh /home/pi/izi-gplx-iot/main.sh > /home/pi/izi-gplx-iot/start_log.txt

Open lại Crontab để config lại startup


# Sơ đồ chân Raspberry Pi 4
![img_4.png](img_4.png)
![img_5.png](img_5.png)


